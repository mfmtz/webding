import styles from '../../styles/carolina-reyes/Section2.module.scss';
import { Cormorant_Garamond, Poppins } from 'next/font/google'
const cormorant_garamond = Cormorant_Garamond({
    subsets: ["latin"],
    weight: ["400", "300", "600"],
})

const poppins = Poppins({
    subsets: ["latin"],
    weight: ["400", "300", "600"],
});;

export default function Section2() {
    return (
        <div className={styles.container}>
            <div className={`${styles.title} ${cormorant_garamond.className}`}>
                EN COMPAÑÍA DE NUESTROS PADRES
            </div>

            <div className={styles.container2} >
                <div className={styles.boySection}>
                <div className={`${styles.title2} ${poppins.className}`}>
                        PAPÁS DEL NOVIO
                    </div>
                    <div className={`${styles.title2} ${poppins.className}`}>
                        REYES MAGDALENO VERDIALES FELIX <br></br>
                        LEOVYGILDA ONTIVEROS ASTORGA <br></br>
                    </div>
                </div>

                <div className={styles.girlSection}>
                <div className={`${styles.title2} ${poppins.className}`}>
                        PAPÁS DE LA NOVIA
                    </div>
                    <div className={`${styles.title2} ${poppins.className}`}>
                        FRANCISCO CARDENAS GRAVE <br></br>
                        MARÍA JESÚS TERRAZAS ROMERO <br></br>
                    </div>
                </div>
            </div>


        </div>
    )
}
