import { Button, Card, FormControlLabel, Radio, RadioGroup } from '@mui/material';
import styles from '../../styles/carolina-reyes/Section5.module.scss';
import { Cormorant_Garamond, Poppins } from 'next/font/google'
const cormorant_garamond = Cormorant_Garamond({
  subsets: ["latin"],
  weight: ["400", "300", "600"],
})

const poppins = Poppins({
  subsets: ["latin"],
  weight: ["400", "300", "600"],
});


export default function Section5() {
    return (
        <div className={styles.container}>
            <div className={styles.title} >
                <div className={`${styles.title1} ${cormorant_garamond.className}`}>DRESS CODE</div>
                <div className={`${styles.title2} ${poppins.className}`}>ETIQUETA</div>


            </div>

            <div className={styles.container2}>

                <div className={styles.men} >
                    <div className={styles.container2Title}>Hombres</div>
                    <div>Traje completo / Corbata o moño</div>
                </div>
                <div className={styles.middle} >1</div>
                <div className={styles.wemen} >
                    <div className={styles.container2Title}>Mujeres</div>
                    <div>Vestido largo</div>
                </div>

            </div>


        </div>
    )
}
