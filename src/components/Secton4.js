import { Button, FormControlLabel, Radio, RadioGroup } from '@mui/material';
import styles from '../../styles/carolina-reyes/Section4.module.scss';
import TextField from '@mui/material/TextField';
import { requestAPI } from '../utils/api';
import { useState, useEffect } from 'react'; // Import useEffect
import constants from "../utils/constants";

export default function Section2() {
  const [guestName, setGuestName] = useState('');
  const [selectedRadio, setSelectedRadio] = useState(0);
  const [messageStyle, setMessageStyle] = useState('none'); // Replace "none" with your initial value


  const loadData = async () => {


    let response = await requestAPI({
      host: constants.HOST, // Make sure to import the 'constants' file
      method: 'PATCH',
      resource: 'mail',
      endpoint: '/confirmation',
      data: {
        name: guestName.trim(),
        guests: selectedRadio,
      },
      headers: {},
    });

    console.log(response);

    if (response?.status === 201) {
      setMessageStyle('block');
    }
  };



  return (
    <div className={styles.container}>
      <div className={styles.title}>CONFIRMA TU ASISTENCIA</div>
      <div className={styles.container2}>
        <div className={styles.asistiras}>
          <div className={styles.asistirasTitle}>¿Asistirás?</div>
          <div className={styles.radioGroup}>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
              value={selectedRadio}
              onChange={(e) => setSelectedRadio(parseInt(e.target.value, 10))}
            >
              <FormControlLabel
                value={0}
                control={<Radio />}
                label="No asistiré"
              />
              <FormControlLabel
                value={1}
                control={<Radio />}
                label="Si asistiré"
              />
            </RadioGroup>
          </div>
          <div className={styles.textfield}>
            <TextField
              onChange={(e) => {
                setGuestName(e.target.value);
              }}
              className={styles.input}
              style={{ }}
              placeholder="Nombre de invitado"
              size="small"
              id="standard-basic"
              variant="outlined"
            />
          </div>
          <div className={styles.textfield}>
            <Button
              onClick={() => {
                loadData();
              }}
              style={{ }}
              className={styles.button}
              variant="contained"
            >
              Confirmar
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
