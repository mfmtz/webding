import styles from '../../styles/carolina-reyes/Section1.module.scss';

export default function Section1() {
  return (
    <div className={styles.container}>
      <img src='./assets/carolina-reyes/homeImage.jpeg' className={styles.image} ></img>
    </div>
  )
}
