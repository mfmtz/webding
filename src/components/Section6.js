import { Button, Card, FormControlLabel, Radio, RadioGroup } from '@mui/material';
import styles from '../../styles/carolina-reyes/Section6.module.scss';
import { Cormorant_Garamond, Poppins } from 'next/font/google'
const cormorant_garamond = Cormorant_Garamond({
    subsets: ["latin"],
    weight: ["400", "300", "600"],
})

const poppins = Poppins({
    subsets: ["latin"],
    weight: ["400", "300", "600"],
});;

export default function Section5() {
    return (
        <div className={styles.container}>
            <div className={`${styles.title} ${cormorant_garamond.className}`}>
                NO NIÑOS
            </div>

            <div className={`${styles.container2} ${poppins.className}`}>
                Amamos a sus pequeños, pero queremos que este día sólo tengan que 
                preocuparse por pasarla increíble y lo dejen todo en la pista.
            </div>


        </div>
    )
}
