import React from "react";
import styles from "../../styles/carolina-reyes/TimeLine.module.scss";
import Stack from "@mui/material/Stack";
import Slider from "@mui/material/Slider";
import { Button } from "@mui/material";
import { Cormorant_Garamond, Poppins } from 'next/font/google'
const cormorant_garamond = Cormorant_Garamond({
  subsets: ["latin"],
  weight: ["400", "300", "600"],
})

const poppins = Poppins({
  subsets: ["latin"],
  weight: ["400", "300", "600"],
});

function valuetext(value) {
  return ``;
}

const information = [
  {
    id: 1,
    element: 1,
    title: 'Misa: Iglesia del Carmen',
    hour: '6:00 PM',
    icon: '',
    url: '/assets/carolina-reyes/church.png'
  },
  {
    id: 2,
    element: 2,
    title: 'Recepción: Salón Madison',
    hour: '9:00 PM',
    icon: '',
    url: '/assets/carolina-reyes/drinks.png'
  },
  {
    id: 3,
    element: 3,
    title: 'Civil: Salón Madison',
    hour: '9:30 PM',
    icon: '',
    url: '/assets/carolina-reyes/engange.png'
  },
];


const getIcon = (number) => {
  switch (number) {
    case 0:
      return (
        <img src={information[number].url} style={{ width: '90px', fontSize: '3.7em', marginTop: '10px', marginBottom: '10px' }} />
      );
    case 1:
      return (
        <img src={information[number].url} style={{ width: '90px', fontSize: '3.7em', marginTop: '10px', marginBottom: '10px' }} />
      );
    case 2:
      return (
        <img src={information[number].url} style={{ width: '90px', fontSize: '3.7em', marginTop: '10px', marginBottom: '10px' }} />
      );
    // Add more cases for different numbers and icons if needed
    default:
      return null; // Return null by default if the number doesn't match any case
  }
};

const MyTimeline = () => {
  return (
    <div className={`${styles.container} ${poppins.className}`}>
    <div className={`${styles.title} ${cormorant_garamond.className}`}>
        <div style={{ textAlign: 'center', fontSize: '2em', marginBottom: '100px' }}>Te compartimos los detalles <br></br> de la celebración</div>
      </div>

      <div className={styles.timeline}>
        <div className={styles.timelineLeft}>
          <Stack sx={{ height: "100%" }} spacing={1} direction="row">
            <Slider
              getAriaLabel={() => ''}
              orientation="vertical"
              defaultValue={[20, 60, 100]}
              valueLabelDisplay="off"
              disabled
              size="small"
              color="primary"
            />
          </Stack>
        </div>

        <div className={styles.timelineRight}>
          {information.map((item, index) => (
            <div key={index} style={(index === 0) ? {} : { marginTop: '100px' }} >
              <div>{item.hour}</div>
              <div style={{ fontSize: '1.3em' }}>{item.title}</div>
              <div>{getIcon(index)}</div>
              <Button className={` ${poppins.className}`} style={{ background: 'black' }}  variant="contained">Ver Dirección</Button>

            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default MyTimeline;
