const constants = {
    CONFIG_ENV: process.env.NODE_ENV,
    HOST: (process.env.NODE_ENV === "development") ? 'http://localhost:3001' : 'http://localhost:3001'
  };
  
  export default constants;
  