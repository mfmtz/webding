const axios = require("axios");


export const requestAPI = async (request) => {
  try {
    console.log('This is the request',request);
    let data = null;
    if (request.data !== undefined) {
      data = request.data;
    }

    let endpoint = request.endpoint || "";
    let URL = request.host + "/api/" + request.resource + endpoint;

    let options = {
      method: request.method,
      url: URL,
      data: { data: data },
    };
    
    console.log('execute axios before');
    const response = await axios(options);
    console.log('execute axios after');

    return response;
  } catch (error) {
    console.log(error);
  }
};


