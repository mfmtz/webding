
import Section1 from '../src/components/Section1';
import Section3 from '../src/components/Section3';
import Section2 from '../src/components/Secton2';
import Section4 from '../src/components/Secton4';
import Section5 from '../src/components/Section5';
import Section6 from '../src/components/Section6';
import Divider from '../src/components/Divider';

export default function Home() {
  return (
    <div>
        <Section1></Section1>
        <Section2></Section2>
        <Divider></Divider>
        <Section3></Section3>
        {/* <Section4></Section4> */}
        <Section5></Section5>
        <Divider></Divider>
        <Section6></Section6>
    </div>
  )
}
